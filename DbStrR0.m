function [R0, Pol] = DbStrR0(x, y, a, b, c, d, u, alpha, beta, K, N, M, varargin)

% Copyright (c) 2020-2021 Simone De Reggi.
% DoubleStructureR0 is distributed under the terms of the MIT license.
%
% If you use this software for a scientific publication,
% please cite the following publication:
%
% [1] D. Breda, S. De Reggi, F. Scarabel, R. Vermiglio, J. Wu,
%     Bivariate collocation for computing R0 in epidemic models with two structures,
%     Computers & Mathematics with Applications, https://doi.org/10.1016/j.camwa.2021.10.026.
%
% [R0, Pol] = DoubleStructureR0(x, y, a, b, c, d, u, alpha, beta, K, N, M)
% computes the approximation for R0 and the relevant eigenfunction
% according to the method described in [1] using N+1 Chebyshev extremal 
% nodes in the interval  [x(1), x(end)] and M+1 Chebyshev extremal 
% nodes in the interval [y(1), y(end)]. The boundary conditions are
% prescribed at x(1) and y(1) respectively. Consult the ''CALL'' paragraph
% for their modification. If piecewise - defined functions are taken into 
% account as parameters for the model, consider using PCWDoubleStrR0_1 and 
% PCWDoubleStrR0_2 for more accurate results.
%
%  INPUT:
%         x,y = vectors 
%   a,b,c,d,u = functions of two variables 
% alpha, beta = functions of three variables
%           K = function of four variables
%           N = degree of the approximating polynomial in [x(1), x(end)]
%           M = degree of the approximating polynomial in [y(1), y(end)]
%      
%  OUTPUT:
%          R0 = approximation of R0
%         Pol = (N+1)(M+1) x (N+1)(M+1) matrix containing the coefficients of the eigenfunction
%               relevant to R0 in its bivariate Lagrange representation
%  CALL:
%        >>[R0, Pol] = DoubleStructureR0(x, y, a, b, c, d, u, alpha, beta, K, 20, 20)
%         
%        For imposing the boundary conditions at x(1) and y(1)
%        >>[R0, Pol] = DoubleStructureR0(x, y, a, b, c, d, u, alpha, beta, K, 20, 20, 1)
%        For imposing the boundary conditions at x(1) and y(end)
%        >>[R0, Pol] = DoubleStructureR0(x, y, a, b, c, d, u, alpha, beta, K, 20, 20, 2)
%        For imposing the boundary conditions at x(end) and y(end)
%        >>[R0, Pol] = DoubleStructureR0(x, y, a, b, c, d, u, alpha, beta, K, 20, 20, 3);

if isempty(varargin)
  index = 1;
else
  index = varargin{1};
end
switch index 
  case 1    
   [xpoint,w]=fclencurt(N+1,x(1),x(end)); 
   [ypoint,v]=fclencurt(M+1,y(1),y(end)); 
    M0 = MMat(xpoint, ypoint, w, v, a, b, c, d, alpha, beta, u, N, M, 1);
    B0 = BMat(xpoint, ypoint, w, v, K, N, M, 1);
   [Psi, MatEig]=eigs(B0/M0);  
    Phi=M0\Psi;
    e=diag(MatEig);
   [R0, P]=max(e);
    Pol=reshape(Phi(:, P), M+1, N+1);         
  case 2    
   [xpoint,w]=fclencurt(N+1,x(1),x(end)); 
   [ypoint,v]=fclencurt(M+1,y(1),y(end)); 
    M0 = MMat(xpoint, ypoint, w, v, a, b, c, d, alpha, beta, u, N, M, 2);
    B0 = BMat(xpoint, ypoint, w, v, K, N, M, 2);
   [Psi, MatEig]=eigs(B0/M0);  
    Phi=M0\Psi;
    e=diag(MatEig);
   [R0, P]=max(e);
    Pol=reshape(Phi(:, P), M+1, N+1);         
  case 3    
   [xpoint,w]=fclencurt(N+1,x(1),x(end)); 
   [ypoint,v]=fclencurt(M+1,y(1),y(end)); 
    M0 = MMat(xpoint, ypoint, w, v, a, b, c, d, alpha, beta, u, N, M, 3);
    B0 = BMat(xpoint, ypoint, w, v, K, N, M, 3);
   [Psi, MatEig]=eigs(B0/M0);  
    Phi=M0\Psi;
    e=diag(MatEig);
   [R0, P]=max(e);
    Pol=reshape(Phi(:, P), M+1, N+1);         
 end
end  

 %% Discretization of the birth operator

function [B0] = BMat(xpoint, ypoint, w, v, K, N, M, index)
  
%Computes the discretization of the birth operator.
%B0 is an (N+1)(M+1) x (N+1)(M+1) matrix
  
[X, Y]=meshgrid(xpoint, ypoint);  
X=reshape(X, [], 1);
Y=reshape(Y, [], 1);
weights=kron(w, v);  

%Evaluating K at a Grid
%Does not work correctly if K is piecewise-defined

B0=K(X, Y, X', Y');
B0=B0.*weights;

%If K is constant in the first two variables or in the latters, 
%B0=K(X, Y,X', Y') returns a vector or a scalar

[s1, s2]=size(B0);   
if s1==1
  B0=repmat(B0, (N+1)*(M+1), 1);
end
if s2==1
  B0=repmat(B0, 1, (N+1)*(M+1));
end

%B0 is now a matrix (N+1)*(M+1)x(N+1)*(M+1)

switch index
  case 1  
    B0(1:M+1, :)=zeros(M+1, (N+1)*(M+1));
    for i=1:N
      B0(i*(M+1)+1, :)=zeros(1, (N+1)*(M+1));
    end
  case 2
    B0(1:M+1, :)=zeros(M+1, (N+1)*(M+1));
    for i=2:N+1
      B0(i*(M+1), :)=zeros(1, (N+1)*(M+1));
    end
  case 3
    B0(N*(M+1)+1:(N+1)*(M+1), :)=zeros(M+1, (N+1)*(M+1));
    for i=1:N
      B0(i*(M+1), :)=zeros(1, (N+1)*(M+1));
    end
end  

B0=sparse(B0);

end

 %% Discretization of the mortality operator
 
function [M0] = MMat(xpoint, ypoint, w, v, a, b, c, d, alpha, beta, u, N, M, index)

% computes the discretization of the mortality operator 
% M0 is an (N+1)(M+1) x (N+1)(M+1) matrix

%for evaluating a, b, c, d and u

[X, Y]=meshgrid(xpoint, ypoint); 

%DL1 is an (N+1)*(N+1) matrix

DL1 = difmat(xpoint);            

%DL2 is an (M+1)*(M+1) matrix

DL2 = difmat(ypoint);
  
switch index 
  case 1  
  %% Discretization of the term with the derivative in the first variable                                               
    A=eye(M+1);                      
    A(1,1)=0;
    L=kron(DL1(2:N+1, :), A);                                               
 
    %Evaluating b at a grid
    %Does not work correctly if b is piecewise-defined
 
    B=b(X, Y);                        
    
    %If b is constant, b(X, Y) is a scalar
    
    if isscalar(B)                    
      L=B*L;               
    else
      B=reshape(B, (N+1)*(M+1), 1)';
      L=L.*B;
    end
    L=[zeros(M+1, (N+1)*(M+1)); L];
    
    %Evaluating a at a grid
    %Does not work correctly if a is piecewise-defined
     
    A=a(X, Y);                      
    
    %If a is constant, a(X, Y) is a scalar
    
    if isscalar(A)                   
      L=A*L;               
    else
      A=reshape(A, (N+1)*(M+1), 1);
      L=A.*L;
    end

  %% Discretization of the term with the derivative in the second variable                                               
    
    G=DL2;              
    G(1, :)=zeros(1, M+1);            
    E=eye(N+1);
    E(1, 1)=0;            
    G=kron(E, G);                                
    
    %Evaluating d at a grid
    %Does not work correctly if d is piecewise-defined
     
    D=d(X, Y);            
    
    %if d is constant, d(X, Y) is a scalar
    
    if isscalar(D)        
      G=D*G;               
    else
      D=reshape(D, (N+1)*(M+1), 1)';
      G=G.*D;
    end   
    
    %Evaluating c at a grid
    %Does not work correctly if c is piecewise-defined
 
    C=c(X, Y);             

    %if c is constant, c(X, Y) is a scalar

    if isscalar(C)       
      G=C*G;               
    else
      C=reshape(C, (N+1)*(M+1), 1);
      G=C.*G;
    end

    %% Discretization of the term in u(x, y)                                                       

    %Evaluating u at a grid
    %Does not work correctly if u is piecewise-defined

    U=u(X, Y);

    %If u is constant, u(X, Y) is a scalar
    
    if isscalar(U)           
      U=U*eye((N+1)*(M+1));
      U(1:M+1, :)=zeros(M+1, (N+1)*(M+1));
      for i=1:N                   
        U(i*(M+1)+1, :)=zeros(1, (N+1)*(M+1));
      end
    else
      U=reshape(U, (N+1)*(M+1), 1);
      U(1:M+1)=0;
      for i=1:N                  
        U(i*(M+1)+1)=0;
      end
    U=U.*eye((N+1)*(M+1), (N+1)*(M+1));
    end 

    %% Boundary condition at x(1)                                                   
 
    X1=reshape(X, [], 1);
    Y1=reshape(Y, [], 1); 
    weights=kron(w, v);
    A1=[eye(M+1), zeros(M+1, N*(M+1))];

    %Evaluating beta at a grid
    %Does not work correctly if beta is piecewise-defined
    
    S=beta(ypoint', X1', Y1');
    [s1, s2]=size(S);
    
    %If beta is constant in the first variable or in the latters, 
    %S=beta(ypoint, ,X1', Y1') returns a vector or a scalar

    if s1==1
      S=repmat(S, M+1, 1);
    end
    if s2==1
      S=repmat(S, 1,(M+1)*(N+1));
    end
 
    S=S.*weights;    
    W=[A1-S; zeros(N*(M+1), (N+1)*(M+1))];

  %% Boundary condition at y(1)                                                   
    Q=zeros((N+1)*(M+1));
    for i=1:N
      Q(i*(M+1)+1, i*(M+1)+1)=1;
    end

    %Evaluating alpha at a grid
    %Does not work correctly if alpha is piecewise-defined    
    
    P=alpha(xpoint', X1', Y1');
    [s1, s2]=size(P);   

    %If alpha is constant in the first variable or in the latters, 
    %P=alpha(xpoint, ,X1', Y1') returns a vector or a scalar
    
    if s1==1
      P=repmat(P, N+1, 1);
    end
    if s2==1
      P=repmat(P, 1, (N+1)*(M+1));
    end
    
    P=P.*weights;    
    Z=zeros(M+1, 1);
    Z(1)=1;    
    P=kron(P, Z);
    P(1:N+1, :)=zeros(N+1, (N+1)*(M+1));    
    V=Q-P;  
    
  case 2   
  %% Discretization of the term with the derivative in the first variable                                               
    A=eye(M+1);                      
    A(M+1,M+1)=0;
    L=kron(DL1(2:N+1, :), A);                                               
    
    %Evaluating b at a grid
    %Does not work correctly if b is piecewise-defined

    B=b(X, Y);                      
    
    %If b is constant, b(X, Y) is a scalar    
    
    if isscalar(B)                
      L=B*L;               
    else
      B=reshape(B, (N+1)*(M+1), 1)';
      L=L.*B;
    end
     L=[zeros(M+1, (N+1)*(M+1)); L];    
    
    %Evaluating a at a grid
    %Does not work correctly if a is piecewise-defined
          
    A=a(X, Y);                   

    %If a is constant, a(X, Y) is a scalar    
       
    if isscalar(A)                    
      L=A*L;               
    else
      A=reshape(A, (N+1)*(M+1), 1);
     L=A.*L;
    end

  %% Discretization of the term with the derivative in the second variable                                               
    G=DL2;              
    G(M+1, :)=zeros(1, M+1);            
    E=eye(N+1);
    E(1, 1)=0;            
    G=kron(E, G);           
    
    %Evaluating d at a grid
    %Does not work correctly if d is piecewise-defined
    
    D=d(X, Y);            

    %If d is constant, d(X, Y) is a scalar
    
    if isscalar(D)        
      G=D*G;               
    else
      D=reshape(D, (N+1)*(M+1), 1)';
      G=G.*D;
    end   
    
    %Evaluating c at a grid
    %Does not work correctly if c is piecewise-defined
 
    C=c(X, Y);             

    %If c is constant, c(X, Y) is a scalar
    
    if isscalar(C)       
      G=C*G;               
    else
      C=reshape(C, (N+1)*(M+1), 1);
      G=C.*G;
    end

  %% Discretization of the term in u(x, y)                                                       

    %Evaluating u at a grid
    %Does not work correctly if u is piecewise-defined
    
    U=u(X, Y);
 
    %If u is constant, u(X, Y) is a scalar
    
    if isscalar(U)           
      U=U*eye((N+1)*(M+1));
      U(1:M+1, :)=zeros(M+1, (N+1)*(M+1));
      for i=2:N+1                   
        U(i*(M+1), :)=zeros(1, (N+1)*(M+1));
      end
    else
      U=reshape(U, (N+1)*(M+1), 1);
      U(1:M+1)=0;
      for i=2:N+1                  
        U(i*(M+1))=0;
      end
    U=U.*eye((N+1)*(M+1), (N+1)*(M+1));
    end 
    
  %% Boundary condition at x(1)                                                   
    X1=reshape(X, [], 1);
    Y1=reshape(Y, [], 1); 
    weights=kron(w, v);
    A1=[eye(M+1), zeros(M+1, N*(M+1))];
    
    %Evaluating beta at a grid
    %Does not work correctly if beta is piecewise-defined

    S=beta(ypoint', X1', Y1');
    
    %If beta is constant in the first variable or in the latters, 
    %S=beta(ypoint, ,X1', Y1') returns a vector or a scalar

    [s1, s2]=size(S);
    if s1==1
      S=repmat(S, M+1, 1);
    end
    if s2==1
      S=repmat(S, 1, (N+1)*(M+1));
    end
    
    S=S.*weights;      
    W=[A1-S; zeros(N*(M+1), (N+1)*(M+1))];

  %% Boundary condition at y(end)                                                   
    Q=zeros((N+1)*(M+1));
    for i=2:N+1
      Q(i*(M+1), i*(M+1))=1;
    end
    
    %Evaluating alpha at a grid
    %Does not work correctly if alpha is piecewise-defined

    P=alpha(xpoint', X1', Y1');

    %If alpha is constant in the first variable or in the latters, 
    %P=alpha(xpoint, ,X1', Y1') returns a vector or a scalar
    
    [s1, s2]=size(P);   
    if s1==1
      P=repmat(P, N+1, 1);
    end
    if s2==1
      P=repmat(P, 1, (M+1)*(N+1));
    end
    
    P=P.*weights;  
    Z=zeros(M+1, 1);
    Z(M+1)=1;    
    P=kron(P, Z);
    P(1:N+1, :)=zeros(N+1, (N+1)*(M+1));    
    V=Q-P;    
  
  case 3 
  %% Discretization of the term with the derivative in the first variable                                               
    A=eye(M+1);                       
    A(M+1, M+1)=0;
    L=kron(DL1(1:N, :), A);                                              

    %Evaluating b at a grid
    %Does not work correctly if b is piecewise-defined
    
    B=b(X, Y);                        
    if isscalar(B)                   
      L=B*L;               
    else
      B=reshape(B, (N+1)*(M+1), 1)';
      L=L.*B;
    end
    L=[L; zeros(M+1, (N+1)*(M+1))];                                                                    

    %Evaluating a at a grid
    %Does not work correctly if a is piecewise-defined

    A=a(X, Y);                    
    if isscalar(A)                   
      L=A*L;               
    else
      A=reshape(A, (N+1)*(M+1), 1);
      L=A.*L;
    end

  %% Discretization of the term with the derivative in the second variable                                               
    G=DL2;              
    G(M+1, :)=zeros(1, M+1);             
    E=eye(N+1);
    E(N+1, N+1)=0;            
    G=kron(E, G);                                

    %Evaluating d at a grid
    %Does not work correctly if d is piecewise-defined
    
    D=d(X, Y);            
    if isscalar(D)        
      G=D*G;               
    else
      D=reshape(D, (N+1)*(M+1), 1)';
      G=G.*D;
    end
    
    %Evaluating c at a grid
    %Does not work correctly if c is piecewise-defined

    C=c(X, Y);             
    if isscalar(C)       
      G=C*G;               
    else
      C=reshape(C, (N+1)*(M+1), 1);
      G=C.*G;
    end

  %% Discretization of the term in u(x, y)                                                       

    %Evaluating u at a grid
    %Does not work correctly if u is piecewise-defined
    
    U=u(X, Y);

    %If u is constant, u(X, Y) is a scalar
 
    if isscalar(U)           
      U=U*eye((N+1)*(M+1));
      U(N*(M+1)+1:(N+1)*(M+1), :)=zeros(M+1, (N+1)*(M+1));
      for i=1:N                   
        U(i*(M+1), :)=zeros(1, (N+1)*(M+1));
      end
    else
      U=reshape(U, (N+1)*(M+1), 1);
      U(N*(M+1)+1:(N+1)*(M+1))=0;
      for i=1:N                  
        U(i*(M+1))=0;
      end
    U=U.*eye((N+1)*(M+1), (N+1)*(M+1));
    end 

  %% Boundary condition at x(end)                                                   
    X1=reshape(X, [], 1);
    Y1=reshape(Y, [], 1); 
    weights=kron(w, v);
    A1=[eye(M+1), zeros(M+1, N*(M+1))];

    %Evaluating beta at a grid
    %Does not work correctly if beta is piecewise-defined
  
    S=beta(ypoint', X1', Y1');
    
    %If beta is constant in the first variable or in the latters, 
    %S=beta(ypoint, ,X1', Y1') returns a vector or a scalar

    [s1, s2]=size(S);
    if s1==1
      S=repmat(S, M+1, 1);
    end
    if s2==1
      S=repmat(S, 1,(M+1)*(N+1));
    end
    S=S.*weights;  
    W=[zeros(N*(M+1), (N+1)*(M+1)); A1-S]; 

  %% Boundary condition at y(end)                                                   
    Q=zeros((N+1)*(M+1));
    for i=1:N
      Q(i*(M+1), i*(M+1))=1;
    end
    
    %Evaluating alpha at a grid
    %Does not work correctly if alpha is piecewise-defined
    
    P=alpha(xpoint', X1', Y1');

    %If alpha is constant in the first variable or in the latters, 
    %P=alpha(xpoint, ,X1', Y1') returns a vector or a scalar
    
    [s1, s2]=size(P);   
    if s1==1
      P=repmat(P, N+1, 1);
    end
    if s2==1
      P=repmat(P, 1, (N+1)*(M+1));
    end    
    P=P.*weights;  
    Z=zeros(M+1, 1);
    Z(M+1)=1;    
    P=kron(P, Z);
    P(N*(M+1)+1:(N+1)*(M+1), :)=zeros(M+1, (N+1)*(M+1));    
    V=Q-P;  
end

%% Computing M0     
M0=L+G+U+W+V;
M0=sparse(M0);

end
