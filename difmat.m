function DL = difmat(xpoint)
  
% Computes the differentiation matrix relevant to the Chebyshev extremal 
% points contained in xpoint according to [1].
%
% INPUT:
%     xpoint = row vector containing the Chebyshev extremal nodes 
%              in the interval [xpoint(1), xpoint(end)]
%                                
% OUTPUT:
%         DL = differentiation matrix relevant to xpoint 
%     
% CALL:
%       >>DL = difmat(xpoint)
%
%
% REFERENCES: 
%            [1] L.N. Trefethen, "Spectral methods in Matlab", SIAM, 2000, 
%                DOI: 10.1137/1.9780898719598.

N = length(xpoint)-1;
x=xpoint';
c=[2;ones(N-1,1);2].*(-1).^(0:N)';
X=repmat(x,1,N+1);
dX=X-X';
DL=(c*(1./c)')./(dX+(eye(N+1)));
DL=DL-diag(sum(DL'));    

end
