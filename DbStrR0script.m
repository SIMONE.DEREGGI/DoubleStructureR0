% Script for R0 in models with two structures

%% Examples for the general model

%% Example 1 - DoubleStrR0_1 required
x=[0, 1]; y=[pi/6, pi/4]; a=@(x, y) cos(y)/3; b=@(x, y) 1; c=@(x, y) sin(y)/3;
d=@(x, y) 1; u=@(x, y) cos(y)/3; K=@(x, y, xi, sigma) exp(x).*cos(y).*sin(y);
C=2*((exp(1)-1)*(sqrt(3)-sqrt(2)))^(-1); alpha=@(x, xi, sigma) C*exp(x)/2;
beta=@(y, xi, sigma) C*sin(y); genEigenfunction=@(x, y) exp(x).*sin(y);
R0=1/C;
[R0, Pol] = DoubleStrR0_1(x, y, a, b, c, d, u, alpha, beta, K, 50, 50)

%% Example 2 - DoubleStrR0_1 required
x=[0, 1]; y=[0, 1]; a=@(x, y) 2*x/15; b=@(x, y) 1; c=@(x, y) y/8;
d=@(x, y) 1; u=@(x, y) 1/3; K=@(x, y, xi, sigma) x.^(5/2).*y.^(8/3);
alpha=@(x, xi, sigma) 0; beta=@(y, xi, sigma) 0; genEigenfunction=@(x, y) x.^(5/2).*y.^(8/3);
R0=6/77;
[R0, Pol] = DoubleStrR0_1(x, y, a, b, c, d, u, alpha, beta, K, 50, 50)

%% Example 3 - DoubleStrR0_1 required
x=[0, 1]; y=[0, 2]; a=@(x, y) 1; b=@(x, y) 1; c=@(x, y) 2*y/7;
d=@(x, y) 1; u=@(x, y) 1; K=@(x, y, xi, sigma) exp(-x).*y.^(7/2); C=9*exp(1)*(2^(11/2)*(exp(1)-1))^(-1);
alpha=@(x, xi, sigma) 0; beta=@(y, xi, sigma) C*y.^(7/2); genEigenfunction=@(x, y) exp(-x).*y.^(7/2);
R0=1/C;
[R0, Pol] = DoubleStrR0_1(x, y, a, b, c, d, u, alpha, beta, K, 50, 50)


%% Examples for the age-immunity model

%% Example with explicit R0 and generalized eigenfunction  - DoubleStrR0_2 required
x=[0, 2]; y=[0, 1]; a=@(x, y) 1; b=@(x, y) 1; c=@(x, y) 0; d=@(x, y) 0; 
d=@(x, y) 0; u=@(x, y) 2; s=@(x, y) exp(-4*x).*(1-y).^2; Beta=@(y) 1-y;
nu=@(y) 1-y; K=@(x, y, xi, sigma) Beta(y).*nu(sigma).*s(x, y);
alpha=@(x, xi, sigma) 0; beta=@(y, xi, sigma) 0; genEigenfunction=@(x, y) exp(x).*sin(y);
R0_real=(1/20)*(exp(-8)*0.5-exp(-4)+0.5);
[R0, Pol] = DoubleStrR0_2(x, y, a, b, c, d, u, alpha, beta, K, 50, 50)

%% Example with unknown R0  - PCWDoubleStrR0_2 required
x=[0, 1.99999]; y=[0, 1]; a=@(x, y) 1; b=@(x, y) 1; c=@(x, y) 0; d=@(x, y) 0; 
u=@(x, y) 1+1./((2-x).^2); Beta=@(y) 1-y; nu=@(y) 1-y; 
K=@(x, y, xi, sigma) Beta(y).*nu(sigma).*functioncases(x, y);
alpha=@(x, xi, sigma) 0; beta=@(y, xi, sigma) 0;
[R0, Pol] = PCWDoubleStrR0_2(x, y, a, b, c, d, u, alpha, beta, K, 50, 50);

function [s]=functioncases(x, y)
  if exp(-x)<=y
    s=0;
  else
    s=(1-y.*exp(x)).^2.*exp(x).*exp(1/2).*exp(-1./(2-x));
  end
end
