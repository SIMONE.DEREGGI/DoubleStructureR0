function [R0, Pol] = PCWDoubleStrR0_1(x, y, a, b, c, d, u, alpha, beta, K, N, M)

%  Copyright (c) 2020-2021 Simone De Reggi.
%  PCWDoubleStrR0_1 is distributed under the terms of the MIT license.
%
%  If you use this software for a scientific publication,
%  please cite the following publication:
%
%  [1] D. Breda, S. De Reggi, F. Scarabel, R. Vermiglio, J. Wu,
%      Bivariate collocation for computing R0 in epidemic models with two structures,
%      Computers & Mathematics with Applications, https://doi.org/10.1016/j.camwa.2021.10.026.
%
%  [R0, Pol] = PCWDoubleStrR0_1(x, y, a, b, c, d, u, alpha, beta, K, N, M)
%  computes the approximation for R0 and the relevant eigenfunction
%  according to the method described in [1] using N+1 Chebyshev extremal 
%  nodes in the interval  [x(1), x(end)] and M+1 Chebyshev extremal 
%  nodes in the interval [y(1), y(end)]. The boundary conditions are
%  prescribed at x(1) and y(1) respectively. 
%  
%   INPUT:
%         x,y = vectors 
%   a,b,c,d,u = functions of two variables 
% alpha, beta = functions of three variables
%           K = function of four variables
%           N = degree of the approximating polynomial in [x(1), x(end)]
%           M = degree of the approximating polynomial in [y(1), y(end)]
%         
%   OUTPUT:
%           R0 = approximation of R0
%          Pol = (N+1)(M+1) x (N+1)(M+1) matrix containing the coefficients of the eigenfunction
%                related to R0 in its bivariate Lagrange representation
%   CALL:
%         >>[R0, Pol] = PCWDoubleStrR0_1(x, y, a, b, c, d, u, alpha, beta, K, 20, 20)

  [xpoint,w]=fclencurt(N+1,x(1),x(end)); 
  [ypoint,v]=fclencurt(M+1,y(1),y(end)); 
   M0 = MMat(xpoint, ypoint, w, v, a, b, c, d, alpha, beta, u, N, M);
   B0 = BMat(xpoint, ypoint, w, v, K, N, M);
  [Psi, MatEig]=eigs(B0/M0);
   Phi=M0\Psi;
   e=diag(MatEig);
  [R0, P]=max(e);
   Pol=reshape(Phi(:, P), M+1, N+1);   

end

 %% Discretization of the birth operator

function [B0] = BMat(xpoint, ypoint, w, v, K, N, M)
  
%computes the discretization of the birth operator 
%B0 is an (N+1)(M+1) x (N+1)(M+1) matrix

[X, Y]=meshgrid(xpoint, ypoint);  
X=reshape(X, [], 1);
Y=reshape(Y, [], 1);
weights=kron(w, v);  

B0=zeros((N+1)*(M+1));

for i=1:N                               
  for j=2:M+1
    for k=1:(N+1)*(M+1)
      B0(i*(M+1)+j, k)=weights(k)*K(xpoint(i+1), ypoint(j), X(k), Y(k));
    end
  end
end
B0=sparse(B0);

end

 %% Discretization of the mortality operator
 
function [M0] = MMat(xpoint, ypoint, w, v, a, b, c, d, alpha, beta, u, N, M)

% computes the discretization of the mortality operator 
% M0 is an (N+1)(M+1) x (N+1)(M+1) matrix

[X, Y]=meshgrid(xpoint, ypoint); 
X=reshape(X, [], 1);
Y=reshape(Y, [], 1); 
weights=kron(w, v);  
DL1 = difmat(xpoint);
DL2 = difmat(ypoint);

%% Discretization of the term with the derivative in the first variable                                               

A=eye(M+1);                       
A(1,1)=0;
L1=kron(DL1(2:N+1, :), A);       

for n=0:N                        
  for m=1:M+1
    L1(:, n*(M+1)+m) = L1(:, n*(M+1)+m)*b(xpoint(n+1), ypoint(m)); 
  end
end

L=[zeros(M+1, (N+1)*(M+1)); L1];  

for i=1:N                         
  for j=2:M+1
    L(i*(M+1)+j, :) = L(i*(M+1)+j, :).*a(xpoint(i+1), ypoint(j)); 
  end
end

%% Discretization of the term with the derivative in the second variable                                               

G=DL2;              
G(1, :)=0;         
E=eye(N+1);
E(1, 1)=0;            
G=kron(E, G);       

for i=1:N            
  for j=2:M+1
    for m=1:M+1  
     G(i*(M+1)+j, i*(M+1)+m)=G(i*(M+1)+j, i*(M+1)+m)*d(xpoint(i+1), ypoint(m));
    end
  end
end

for i=1:N
  for j=2:M+1
    G(i*(M+1)+j, :)=G(i*(M+1)+j, :)*c(xpoint(i+1), ypoint(j));    
  end
end

%% Discretization of the term in u(x, y)                                                       

U=zeros((N+1)*(M+1));
for i=1:N                     
  for j=2:M+1
    U(i*(M+1)+j, i*(M+1)+j)=u(xpoint(i+1),ypoint(j));  
  end              
end

%% Boundary condition at x(1)                                                   

A=[eye(M+1), zeros(M+1, N*(M+1))];
S=zeros(M+1, (N+1)*(M+1));   
for k=1:M+1                   
  for i=1:(N+1)*(M+1)
    S(k, i)=weights(i)*beta(ypoint(k), X(i), Y(i));
  end              
end
W=A-S;    
W=[W; zeros(N*(M+1), (N+1)*(M+1))]; 

%% Boundary condition at y(1)                                                   

Q=zeros((N+1)*(M+1));
for i=1:N
  Q(i*(M+1)+1, i*(M+1)+1)=1;
end

P=zeros((N+1)*(M+1));

for k=1:N                            
  for i=1:(N+1)*(M+1)
    P(k*(M+1)+1, i)=weights(i)*alpha(xpoint(k+1), X(i), Y(i));  
  end              
end

V=Q-P;    

%% Computing  M0                                                          

M0=L+G+U+W+V;
M0=sparse(M0);

end
