# DoubleStructureR0

DoubleStructureR0 is a package for the approximation of R0 in population models with two internal structures formulated as PDEs.

This repository contains the MATLAB codes for the pseudospectral discretization of the Next Generation Operator in population models with two internal structures formulated as PDEs and the computation of R0 as its spectral radius. 

# Description of the codes

## DbStrR0.m 
Computes the pseudospectral discretization of the next generation operator by discretizing the associated generalized eigenvalue problem. Then computes R0 as its spectral radius. DbStrR0.m allows to modify the extremal points at which the boundary conditions are imposed. Does not work correctly if piecewise-defined coefficients for the model are taken into account.

## DoubleStrR0_1.m 
Computes the pseudospectral discretization of the next generation operator by discretizing the associated generalized eigenvalue problem. Then computes R0 as its spectral radius. Given the rectangle [x(1), x(end)] x [y(1), y(end)], DoublebStrR0.m imposes the boundary conditions at x(1) and y(1) respectively. Does not work correctly if piecewise-defined coefficients for the model are taken into account.

## DoubleStrR0_1.m 
Computes the pseudospectral discretization of the next generation operator by discretizing the associated generalized eigenvalue problem. Then computes R0 as its spectral radius. Given the rectangle [x(1), x(end)] x [y(1), y(end)], DoublebStrR0.m imposes the boundary conditions at x(1) and y(end) respectively. Does not work correctly if piecewise-defined coefficients for the model are taken into account. 

## PCWDoubleStrR0_1.m 
Computes the pseudospectral discretization of the next generation operator by discretizing the associated generalized eigenvalue problem. Then computes R0 as its spectral radius. Given the rectangle [x(1), x(end)] x [y(1), y(end)], DoublebStrR0.m imposes the boundary conditions at x(1) and y(1) respectively. To be used when piecewise-defined coefficients for the model are taken into account. 

## PCWDoubleStrR0_2.m 
Computes the pseudospectral discretization of the next generation operator by discretizing the associated generalized eigenvalue problem. Then computes R0 as its spectral radius. Given the rectangle [x(1), x(end)] x [y(1), y(end)], DoublebStrR0.m imposes the boundary conditions at x(1) and y(end) respectively. To be used when piecewise-defined coefficients for the model are taken into account. 

## difmat.m
Computes the differentiation matrix relevant to the Chebysbhev extremal nodes on a given interval

## flcencurt.m
Computes the Chebyshev extremal nodes and the Clenshaw-Curtis quadrature weights on a given interval.

# Copyright and licensing

## DoubleStructureR0

Copyright (c) 2020-2021 Simone De Reggi

DoubleStructureR0 is distributed under the terms of the MIT license (see LICENSE.txt).

If you use this software for a scientific publication,
please cite the following publications:

D. Breda, S. De Reggi, F. Scarabel, R. Vermiglio, J. Wu,
Bivariate collocation for computing R0 in epidemic models with two structures,
Computers & Mathematics with Applications, https://doi.org/10.1016/j.camwa.2021.10.026.

-----------

## difmat.m

Copyright (c) 2000 Lloyd N. Trefethen

The code in difmat.m is taken from L. N. Trefethen, Spectral Methods in MATLAB, SIAM, 2000, DOI: 10.1137/1.9780898719598.

Even though the codes therein are not explicitly licensed, considering that they are made available for download on the author’s web page and that 
variations of some of them are included in Chebfun, which is distributed under the 3-clause BSD license, I believe that the author’s intention was 
for his codes to be freely used and that distributing this file together with DoubleStructureR0 does not violate his rights.

-----------

## flcencurt.m

fclencurt, version 1.0.0.0, 02/12/2005, by Greg von Winckel

Copyright (c) 2009, Greg von Winckel

Retrieved on November 17, 2021 from https://www.mathworks.com/matlabcentral/fileexchange/6911-fast-clenshaw-curtis-quadrature

fclencurt is distributed under the terms of the 2-clause BSD license 
